$(document).ready(function() {

	// Extend the UI with International clocks
	function addIntClocks2UI(config) {
		chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_clocks_" + config.length});
		var AddTimeContainer = setInterval(function(){
			if ($('div.title-bar-inner.window-controls').length > 0){
				// iterate in reverse order
				for	(var index = config.length-1; index >= 0; --index) {
				    var cc = config[index];
					$('div.title-bar-inner.window-controls').prepend(cc.name + ':&nbsp;<div id="time' + index + '"></div>&nbsp;&nbsp;&nbsp;&nbsp;');
				}

				setInterval(function()
				{
					for (var item in config) {
					    var cc = config[item];
						$('#time'+item).html(moment().tz(cc.timezone).format(cc.format));
					}
				}, 250);
				clearInterval(AddTimeContainer);
			}
		},500);
    }

	// monitor window size, if width < 600, remove clocks
	function monitorWindowSize()
	{
		$(window).bind('resize', function() {
			if ($(window).width() < 600)
			{
				$('div.title-bar-inner.window-controls').hide();
			}
			else
			{
				$('div.title-bar-inner.window-controls').show();
			}
		});
	}

	// http://stackoverflow.com/questions/3937000/chrome-extension-accessing-localstorage-in-content-script
    chrome.runtime.sendMessage({method: "getLocalStorage", key: 'intclocks-enabled'}, function(response) {
		if (response.data == 'true') {
			chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_clocks"});
			chrome.runtime.sendMessage({method: "getIntClocksConfig"}, function(response) {
				addIntClocks2UI(response);
				monitorWindowSize();
		    });
        }
    });
});
