//$('head').append($('<script>').attr('type', 'text/javascript').attr('src', chrome.extension.getURL('js/jquery.focusEnd.js')));

var ConversationObserver = new MutationObserver(function(mutations) {
	console.log('cex: Calling HotWord detection and replacing content');
	HotWord();
	$.when( HotWord() ).done(HotWordDone());
});

var HotwordJIRAServerUrlInternal = 'https://jira.dev.global-intra.net:8443';
var HotwordJIRAServerUrlPublic = 'https://195.97.14.69:58443';
var HotwordJIRAServerUrl = HotwordJIRAServerUrlInternal;
console.log('cex: HotwordJIRAServerUrl: ' + HotwordJIRAServerUrl);

$.get( HotwordJIRAServerUrl , function( data, stat, xhr ) {
	if (xhr && xhr.status == 200) {
		HotwordJIRAServerUrl = HotwordJIRAServerUrlInternal;
		console.log('cex: using internal JIRA URL at: ' + HotwordJIRAServerUrl);
	} else {
		HotwordJIRAServerUrl = HotwordJIRAServerUrlPublic;
		console.log('cex: using external JIRA URL at: ' + HotwordJIRAServerUrl);
	}
	//(data && data.cod == 200) ? console.log('cex: YES') : console.log('cex: NO')
}).fail(function() {
	HotwordJIRAServerUrl = HotwordJIRAServerUrlPublic;
	console.warn('cex: unknown error while verifying JIRA URL, using external JIRA URL at: ' + HotwordJIRAServerUrl);
});

function WaitingNode(initial)
{
	var tempInterval = setInterval(function()
	{
		if ($("#HotWordTracking").length === 0)
		{
			// trying to add id attribute to div element
			$('div.middle-pane ul.itemlist li').parent('div').attr('id', 'HotWordTracking');
		}
		else if ($('div.middle-pane ul.itemlist li').length > 0)
		{
			clearInterval(tempInterval);
			var targetNode = document.getElementById('HotWordTracking');
			ConversationObserver.observe(targetNode, { childList: true, subtree: true });
			console.log('cex: HotWordTracking id attribute has been added to node - Observer has been started.');

			HotWord();
			$.when( HotWord() ).done(HotWordDone());
			console.log('cex: Initial HotWord detection on loaded content has been started.');

			targetNode.addEventListener ('DOMNodeRemovedFromDocument', function()
			{
				console.log('cex: node containing conversation text has been removed. restarting');
				WaitingNode();
			});
		}
		else if (initial === true)
		{
			console.log('cex: unable to find node on loading page... waiting.');
		}
	}, 250);
};

function HotWord()
{
	var jiraReg 		= new RegExp('"[A-Z]{2,8}-[0-9]+"', 'g');
	//var jiraRegMatch	= $("div.content.post div.content").text().match(jiraReg);
	var jiraRegMatch	= $("div.message-wrapper span").text().match(jiraReg);
	if (jiraRegMatch != null)
	{
		$.each(jiraRegMatch, function( i, v ){
			var reg = new RegExp(v, 'g');
			if (!$("div.message-wrapper span a").text().match(reg))
			{
				$("div.message-wrapper span").each(function() {
					$(this).html(function() {
						return $(this).html().replace(reg, '<a class="hotwordjira" href="' + HotwordJIRAServerUrl + '/browse/' + v.replace(/"/g, '') + '" target="_blank">' + v.replace(/"/g, '') + '</a>');
					});
				});
			}
		});
	}

	var gsiflowReg 		= new RegExp('"NA[0-9]{8}"', 'g');
	var gsiflowRegMatch	= $("div.message-wrapper span").text().match(gsiflowReg);
	if (gsiflowRegMatch != null)
	{
		$.each(gsiflowRegMatch, function( i, v ){
			var reg = new RegExp(v, 'g');
			if (!$("div.message-wrapper span a").text().match(reg))
			{
				$("div.message-wrapper span").each(function() {
					$(this).html(function() {
						return $(this).html().replace(reg, '<a class="hotwordgsiflow" href="http://prdrace1.global-intra.net:8181/ictsft_old_vs/servlet/ft.report?$casenumber$=' + v.replace(/"/g, '') + '&$server$=prdrace1.global-intra.net&$worklogtype$=allworklogs">' + v.replace(/"/g, '') + '</a>');
					});
				});
			}
		});
	}
}

function HotWordDone()
{
	console.log('cex: adding specific actions to all generated anchor elements');
	$( ".hotwordjira" ).unbind('mouseover.hotwordjira');
	$( ".hotwordjira" ).bind('mouseover.hotwordjira', function()
	{
		var object = $(this);
		$.ajax({
			type: "POST",
			url: HotwordJIRAServerUrl + '/rest/api/2/search',
			headers: { 'Access-Control-Allow-Origin': '*' },
			contentType: 'application/json; charset=UTF-8',
			data: '{"jql":"id=' + object.text() + '","startAt":0,"maxResults":1,"fields":["status","summary"]}',
			dataType: "json"
		}).success(function(data)
		{
			console.log('cex: HotWordDone() success() got data: ' + JSON.stringify(data));
			object.tooltipster({
				content: $('<div><img src="' + data.issues[0].fields.status.iconUrl + '" />&nbsp;(' + data.issues[0].fields.status.name + '):&nbsp;' + data.issues[0].fields.summary + '</div>'),
				contentAsHTML: true,
				multiple: true
			});
			object.tooltipster('show');
		}).error(function(data)
		{
			console.log('cex: HotWordDone() error() got data: ' + JSON.stringify(data));
			//HotwordJIRAServerUrl = HotwordJIRAServerUrlPublic;
			//console.log('cex: overriding HotwordJIRAServerUrl: ' + HotwordJIRAServerUrl);
		});
	});

	$( ".hotwordgsiflow" ).unbind('click.hotwordgsiflow');
	$( ".hotwordgsiflow" ).bind('click.hotwordgsiflow', function()
	{
		event.preventDefault();
		var href = $(this).attr('href');
		//alert($(this).attr('href'));
		if (event.shiftKey || window.localStorage['gsiflowUser'] == undefined || window.localStorage['gsiflowPass'] == undefined)
		{
			var statesdemo = {
				state0: {
					title: 'GSI.flow credentials',
					html:'<table><tr><td>Username</td><td><input type="text" name="user" value=""></td></tr>'+
						'<tr><td>Password</td><td><input type="password" name="pass" value=""></td></tr></table>',
					buttons: { OK: 1 },
					focus: "input[name='fname']",
					submit:function(e,v,m,f){
						window.localStorage['gsiflowUser'] = f.user;
						window.localStorage['gsiflowPass'] = f.pass;

						window.open(
							href + "&$username$=" + window.localStorage['gsiflowUser'] + "&$password$=" + window.localStorage['gsiflowPass']
						);
						$.prompt.close();
					}
				},
			};
			$.prompt(statesdemo);
		}
		else
		{
			window.open(
				href + "&$username$=" + window.localStorage['gsiflowUser'] + "&$password$=" + window.localStorage['gsiflowPass']
			);
		}
	});
}

// Hotword enable by user ?
	chrome.runtime.sendMessage({method: "getLocalStorage", key: 'hotword-enabled'}, function(response) {
		if (response.data == 'true') {
			console.log('cex: Hotword enabled.');
			chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_hotword"});
			// initial startup
			WaitingNode(true);

			$(document).on('mouseup', function(event)
			{
				if (window.getSelection)
				{
					var text = window.getSelection().toString().trim();
					var URItext = encodeURIComponent(text).replace(/\"/g, '%22').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
					var HTMLtext = text.replace(/\"/g, '&quot;').replace(/\&/g, '&amp;').replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
					console.log('cex: URItext: ' + text);
					var PLAINtext = text.replace(/(\r\n|\n|\r)/gm,'');
					
					if (text != "" && text != "%0A" && localStorage['lastHotwordSelection'] != text)
					{
						console.log('cex: Hotword: updating last text selected in internal database');
						localStorage['lastHotwordSelection'] = text;
						
						console.log('cex: Hotword: removing old div with id=hotword');
						$('div[id*=hotword]').remove();
						
						var tempID = 'hotword' + parseInt(event.timeStamp);

						$('body').append('<div style="z-index:999; top:' + (event.clientY + 5) + 'px; left:' + event.clientX + 'px; position: absolute;" id="' + tempID + '"></div>');
						var content =	'<img style="cursor: pointer;" onclick="$(\'input[ng-model=query]\').val(\'' + HTMLtext + '\');$(\'input[ng-model=query]\').change();" src="' + chrome.extension.getURL('img/circuit.png') + '" title="Search on Circuit" />&nbsp;&nbsp;' +
										'<img style="cursor: pointer;" onclick="$(\'#input-field\').html($(\'#input-field\').html() + \'<i>&quot;' + HTMLtext + '&quot;</i></br>\');$(\'#input-field\').removeClass(\'placeholder\');$(\'#input-field\').focusEnd();" src="' + chrome.extension.getURL('img/comment.png') + '" title="Quote on Circuit" />&nbsp;&nbsp;' +
										'<img style="cursor: pointer;" onclick="window.open(\'https://translate.google.com/#auto/de/' + URItext + '\')" src="' + chrome.extension.getURL('img/google-translate.png') + '" title="Translate on Google" />&nbsp;&nbsp;' +
										'<img style="cursor: pointer;" onclick="window.open(\'https://google.com/#q=' + URItext + '\')" src="' + chrome.extension.getURL('img/google-web-search.png') + '" title="Search on Google" />&nbsp;&nbsp;' +
										'<img style="cursor: pointer;" onclick="window.open(\'https://wikipedia.org/wiki/Special:Search/' + URItext + '\')" src="' + chrome.extension.getURL('img/wikipedia.png') + '" title="Search on Wikipedia" />&nbsp;&nbsp;' +
										'<img style="cursor: pointer;" onclick="window.open(\'https://google.com/maps/search/' + URItext + '\')" src="' + chrome.extension.getURL('img/map.png') + '" title="Show on Google Maps" />';

						if (PLAINtext.match(/[A-Z]{2,8}-[0-9]+/g))
						{
							content += '&nbsp;&nbsp;<img style="cursor: pointer;" onclick="window.open(\'' + HotwordJIRAServerUrl + '/browse/' + PLAINtext + '\')" src="' + chrome.extension.getURL('img/jira.png') + '" title="Open in Jira" />';
						}
						if (PLAINtext.match(/CQ[0-9]{2,8}/g))
						{
							content += '&nbsp;&nbsp;<img style="cursor: pointer;" onclick="window.open(\'https://clearquestsrv1.dev.global-intra.net/cqweb/main?command=GenerateMainFrame&service=CQ&schema=CQ&contextid=CQ&entityDefName=CQ&entityID=' + PLAINtext + '\')" src="' + chrome.extension.getURL('img/clearquest.png') + '" title="Open in ClearQuest" />';
						}
						
						
						console.log('cex: Hotword: showing hotword selection tooltip' + content);
						$('#' + tempID).tooltipster({
							content: content,
							position: 'bottom',
							restoration: 'none',
							contentAsHTML: true,
							timer: 15000,
							interactive: true,
							autoClose: false,
							onlyOne: true
						});
						$('#' + tempID).tooltipster('show');

						chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_hotword_overlay"});

					}
					else
					{
						console.log('cex: Hotword: previous selected Hotword: ' + localStorage['lastHotwordSelection']);
						localStorage['lastHotwordSelection'] = '';
						$('div[id*=hotword]').remove();
					}
				}
				else
				{
					console.log('cex: Hotword: no text selected on mouse-up event');
					localStorage['lastHotwordSelection'] = '';
					$('div[id*=hotword]').remove();
				}
			});
		}
	});
