$(document).ready(function() {
	// Do not close window by accident when in a call
		window.onbeforeunload = function(e) {
			if ($( "ul.calls li" ).length > 0 && $("audio").attr('ng-src') != "")
			{
				chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_exit_warning"});
				return 'In call with: ' + $( "ul.calls li div.contact-text" ).text();
			}
		};

	// check if notes are enabled
		var NotesEnabled	= false;
		var IntervalRunning	= false;
		chrome.runtime.sendMessage({method: "getLocalStorage", key: 'notes-enabled'}, function(response)
		{
			if (response.data == 'true')
			{
				NotesEnabled = true;
				console.log('cex: Notes enabled.');
				chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_notes"});
			}
		});

	// check if weather is enabled
		var WeatherEnabled	= false;
		chrome.runtime.sendMessage({method: "getLocalStorage", key: 'weather-enabled'}, function(response) {
			if (response.data == 'true') {
				WeatherEnabled = true;
				console.log('cex: Weather enabled.');
				chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_weather"});
			}
		});

	var AddNotesArea = function()
	{
			chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_notes"});

		// get current conversation ID
			var conv = window.location.href.split('/').pop();

		// add NotesArea box to DOM
			$("div.column-content-inner").prepend('<div class="section files"><div class="title ng-binding">Notes</div><textarea id="notesTextArea" style="left: 0px; top: 0px; width: calc(100% - 10%); height: 150px; resize: none; position: relative; display: inline; margin: 0px;"></textarea></div>');

		// get Notes content from local storage
			chrome.runtime.sendMessage({method: "getLocalStorage", key: conv}, function(data) {
				if (typeof data === 'object' && data.data != undefined)
				{
					// if data exists in backgound page add it to the notes box
					$("#notesTextArea").text(decodeURIComponent(data.data));
				}
				else if (window.localStorage[conv] != undefined)
				{
					// if data exists in website storage, add it to notes box and copy it to background page automatically
					$("#notesTextArea").text(window.localStorage[conv]);
					chrome.runtime.sendMessage({method: "writeLocalStorage", data: {key: conv, value: encodeURIComponent(window.localStorage[conv])}});
				}
			});

		// handle unfoce event to save data
			$("#notesTextArea").on("focusout", function() {
				chrome.runtime.sendMessage({method: "analytics", data: "SAVE_notes"});
				chrome.runtime.sendMessage({method: "writeLocalStorage", data: {key: conv, value: encodeURIComponent($("#notesTextArea").val())}});
			});
	}

	// get weather data function
	function GetWeatherData(LocationElement, containerID)
	{
		// possible 'location' strings (Jan 2016!!)
		//   München, Bayern, Germany, 10:23 AM
		//   Berlin, Germany, 10:23 AM
		//   Zolling, 10:23 AM
		//   Sheffield, United Kingdom   (! no time)
		//   Tiverton, Ontario, Canada, 2:44 PM
		var UserLocation = LocationElement.text().split(",");
		console.log('cex: getWeatherData() UserLocation: ' + UserLocation);
		var query= '';
		if (UserLocation.length > 1)
		{
			// cut off time field (at the end)
			if (UserLocation[UserLocation.length-1].indexOf(':') >= 0)
				UserLocation.splice(-1,1);
			if (UserLocation.length >= 1)
			{
				for (index = 0; index < UserLocation.length; ++index) {
					UserLocation[index] = UserLocation[index].trim();
				}
				query = UserLocation.join(',');
			}
			else
			{
				$("#" + containerID).html("");
			}
			
			console.log('cex: getWeatherData() query: ' + query);
			if (query)
			{
				chrome.runtime.sendMessage({method: "getWeather", data: query}, function(res) {
					console.log('cex: getWeatherData() res: ' + JSON.stringify(res));
					if (res.result)
					{
						chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_weather"});
						$("#" + containerID).html('&nbsp;| ' + res.result);
					}
				});
			}
		}
	}

	// create an observer instance, currently only for input fields, maybe more :-)
		var AttachObserver = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {

				// show notes for every conversation
					if (NotesEnabled && IntervalRunning === false && $("#notesTextArea").length === 0)
					{
						IntervalRunning = true;
						var CreateNotesTextArea = setInterval(function()
						{
							if ($("div.column-content-inner").length > 0)
							{
								AddNotesArea();
								console.log('cex: Notes area created on loading conversation');
								clearInterval(CreateNotesTextArea);
								$("div[ng-click*=toggleColumn]").click(function(){
									if ($("#notesTextArea").length === 0) {
										var RestoreNotesTextArea = setInterval(function(){
											if ($("div.column-content-inner").length > 0) {
												AddNotesArea();
												console.log('cex: Notes area restored after opening 3rd column');
												clearInterval(RestoreNotesTextArea);
											}
										},100);
									}
								});

								IntervalRunning = false;
							}
						}, 100);
					}

					// Add weather info
					if (WeatherEnabled)
					{
						console.log('cex: checking weather');
						var DiscoverLocationInformation = setInterval(function()
						{
							// a direct conversation (location in the header), Settings page
							if ($("div.user div[ng-click*=showLocation]").not($("[id*=locationcontainer]")).length == 1)
							{	
								var containerID = moment().unix();
								
								// add id to node holding location data
								$("div.user div[ng-click*=showLocation]").not($("[id*=locationcontainer]")).attr('id', 'locationcontainer' + containerID);
								
								console.log('cex: start watching users location #' + containerID);
								
								// place div for weather data
								$("#locationcontainer" + containerID).after('<div id="' + containerID + '"></div>');

								// get weather at page loading
								GetWeatherData($("#locationcontainer" + containerID), containerID);

								// create MutationObserver
								var LocationObserver = new MutationObserver(function(mutations) {
									mutations.forEach(function(mutation) {
										console.log('cex: Location has changed: #1 ' + $("div.user div[ng-click*=showLocation]").html());
										GetWeatherData($("#locationcontainer" + containerID), containerID);
									});
								});

								LocationObserver.observe(document.getElementById("locationcontainer" + containerID), { characterData: true, subtree: true });

								clearInterval(DiscoverLocationInformation);
							}
						}, 500);
					}
			});
		});

	// start observer on document.body
		AttachObserver.observe(document.body, { childList: true, characterData: true });
});
