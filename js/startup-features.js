// logging Circuit Extender version to console
var manifest = chrome.runtime.getManifest();
console.log(manifest.name + ' v' + manifest.version + ' attached ;-)');

// check if changes are pending
chrome.runtime.sendMessage({method: "writeLocalStorage", data: {key: 'pendingChanges', value: 'false'}});
var pendingRestartInterval = setInterval(function()
{
	chrome.runtime.sendMessage({method: "getLocalStorage", key: 'pendingChanges'}, function(response) {
		if (response.data == 'true') {
			console.log('cex: pending restart required!');
			var output = '<div class="popover bottom in" style="top: 31.5px; left: -200px; display: block; bottom: auto;" id="pendingChangesPopup"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title ng-hide"></h3><div class="popover-content"><div class="snooze"><div class="title"><span>Circuit Extender</span></div><div class="info"><span>Changes are pending and require to reload the Circuit website.</span></div><div class="list"><ul><li><span class="listText" id="reloadNow">Reload now</span></li><li><span class="listText" id="closeNow">Close</span></li></ul></div></div></div></div></div>';
			$('body').append(output);
			var leftOffset = document.querySelector("div.header-icons div.presence").offsetLeft - (document.querySelector("#pendingChangesPopup").offsetWidth / 2) + 12;
			$("#pendingChangesPopup").css('left', leftOffset + 'px');
			$('#reloadNow').click(function()
			{
				window.location.assign('https://' + window.location.host + '/login');
			});
			$('#closeNow').click(function()
			{
				$('#pendingChangesPopup').remove();
			});
			clearInterval(pendingRestartInterval);
		}
	});
}, 1000);

// auto join call feature
if (location.href.match(/joincall/g))
{
	chrome.runtime.sendMessage({method: "analytics", data: "join_call"});
	console.log('cex: User requests to join call directly after conversation has loaded');
	// let loop run for max of 30 seconds
	var i = 0;
	var JoinCallInterval = setInterval(function()
	{
		i += 1;
		if ($('div.middle-pane ul.itemlist li').length > 0)
		{
			if ($( "div.action-button.conference-open" ).length > 0)
			{
				console.log('cex: starting conference call');
				$( "div.action-button.conference-open" ).click();
				clearInterval(JoinCallInterval);
			}
			else if ($( "div.action-button.join" ).length > 0)
			{
				console.log('cex: join conference call');
				$( "div.action-button.join" ).click();
				clearInterval(JoinCallInterval);
			}
		}
		if (i >= 60)
		{
			clearInterval(JoinCallInterval);
			console.log('cex: no call to join. nothing to do');
		}
	}, 500);
}

//start new conversation feature
if (location.href.match(/draftType=PRIVATE/g))
{
	chrome.runtime.sendMessage({method: "analytics", data: "new_conversation"});
	var draftURL = location.href;
	var contact = draftURL.match(/mail=(.*)/);
	if (contact instanceof Array)
	{
		console.log(contact);
		var interval = setInterval(function()
		{
			if (draftURL != location.href)
			{
				location.href = draftURL;
			}
			if ($('div.input-container input').length === 1)
			{
				$('div.input-container input').val(contact[1]);
				$('div.input-container input').change();
				clearInterval(interval);
			}
		}, 200);
	}
}
