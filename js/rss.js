chrome.runtime.sendMessage({method: "getLocalStorage", key: 'rss-enabled'}, function(response) {
	if (response.data == 'true')
	{
		console.log('cex: RSS feed reader enabled');
		chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_rss"});
		chrome.runtime.sendMessage({method: "getLocalStorage", key: 'rss-feed'}, function(response) {
			console.log('cex: RSS feedAddress: ' + response.data);
		} );
		chrome.runtime.sendMessage({method: "verifyRSSaddress"}, function(response) {
			if (response.data == 'true')
			{
				var RSSconvItem='<li class="selector-item list-item text small" id="RSSconvItem">' +
								'<div class="list-item-inner ng-isolate-scope">' +
								'<div class="conversation-item">' +
								'<div class="iconbar"><div class="component avatar medium u-avatar ng-isolate-scope">' +
								'	<img class="contact ng-scope" src="' + chrome.extension.getURL('img/rss.png') + '">' +
								'</div></div>' +
								'<div class="item-body"><div class="title"><div class="contact-name"><div class="ng-scope"><div class="contact-text">' +
								'	<span class="ng-binding" id="RSSTitle">RSS Reader</span>' +
								'</div></div></div>' +
								'<div class="ng-scope"><div class="content with-text"><div class="text">' +
								'	<span class="text-charcoal50 ng-binding" id="NewItemTitle">RSS feed description</span>' +
								'</div></div></div></div></div><div class="metainfobar ng-scope"><div class="dogear"></div>' +
								'	<div class="timestamp" id="MainItemTimestamp">&nbsp;</div>' +
								'	<div class="unread-badge"><span class="unread-counter ng-binding">NEW</span></div>' +
								'</div></div></div></li>';

				//'<div class="component flex-1 ng-scope" id="RSSContainerFull">' +
				var RSScontainer=	'<div class="conversation-feed flex-1 layout vertical ng-scope" id="RSSContainerFull">' +
									'<div class="layout-inner flex-column">' +
									'<div id="fullscreen">' +
									'<div class="learning-mark ng-isolate-scope" placement="bottom" horizontal-offset="18" vertical-offset="60"></div>' +
									'<div class="component feed-header normal">' +
									'<div class="component-inner animate ng-scope">' +
									'<div class="layout horizontal">' +
									'<div class="layout-inner flex-1">' +
									'<div class="animate-avatar">' +
									'<div class="component avatar u-avatar ng-isolate-scope large" id="RSSavatar">' +
									'<img class="contact ng-scope" src="content/images/icon-general-default-avatar-orange-XL.png">' +
									'</div>' +
									'</div>' +
									'<div class="component user">' +
									'<div class="info animate-info">' +
									'<div class="primary primary-title default-mode">' +
									'<div class="conversation-title readonly ng-scope">' +
									'<div class="display-guest-name ng-isolate-scope">' +
									'<span class="name ng-binding" id="GlobalName">RSS feed</span>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'<div class="secondary direct ng-scope">&nbsp;</div>' +
									'<div class="tabs">' +
									'<div class="conversation radio ng-binding"></div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'<hr class="solid_reversed">' +
									'<div class="component conversation-feed-list-container large-list-background flex-1 two-columns">' +
									'<div class="layout vertical flex-1 ng-scope">' +
									'<div class="layout-inner flex-1 feedlist-thirdcolumn-container rendered">' +
									'<div class="middle-pane layout-inner flex-1 dragarea" style="overflow: hidden;">' +
									'<div class="scroll-shadow top show"></div>' +
									'<div class="scrollable-feed-list component feed-list dragarea ng-isolate-scope highlight">' +
									'<div class="flex-1 container ng-scope ng-isolate-scope">' +
									'<div ng-transclude="">' +
									'<ul class="itemlist large ng-scope ng-isolate-scope" id="RSSContainer">' +
									'</ul>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'<div class="scroll-shadow bottom"></div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>';
				//					'</div>';

				var GlobalName = '';

				var cleanupRSSdescription = function(desc)
				{
					//console.log("YYYY: desc" + desc);
					// heise.de
					var pos = desc.indexOf("<br clear='all'/>");
					//console.log("YYYY: pos: " + pos);
					if (pos > 0)
					    desc =  desc.substring(0, pos + 21);

					return desc;
				}

				var createOneRSSitem = function(data)
				{
					//console.log("XXXX: " + data.title);
					//console.log("XXXX: " + data.description);
					//console.log("XXXX: " + data.pubDate);
					var item =	'<li class="list-item text large"><div ng-switch="feed.clType">' +
								'<div class="list-item-inner ng-scope"><div class="feed-item text ng-isolate-scope child">' +
								'<div class="top-modifier ng-scope"><div class="top-modifier-content">' +
								'<div class="comment-on ng-binding">&nbsp;</div><div class="comment-text" onClick="javascript:window.open(\'' + data.link + '\');">' +
								'<span class="ng-scope">' + data.title + '</span>' +
								'</div></div><hr class="solid"></div>' +
								'<div class="item-header-body"><div class="item-header"><div class="title-content-container">' +
								'<div class="content post">' +
								'<div class="content ng-binding">' + cleanupRSSdescription(data.description) + '</div>' +
								'<div class="controls-container ng-scope"><div class="timestamp">' + moment(data.pubDate).format("ddd, MMM Do YYYY, h:mm a") + '</div>' +
								'</div></div></div></div></div></div></div></div></li>';
					return item;
				}

				var showRSSdata = function()
				{
					//console.log('cex: rss: Sending request');
					chrome.runtime.sendMessage({method: 'RSSgetData'}, function(data)
					{
						//console.log('cex: rss: receive data');
						if (data && data.length > 0)
						{
							//console.log('cex: rss: data.length: ' + data.length);
							chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_rss"});

							window.location.assign('https://' + window.location.host + '/#/rss');
							var AddRSSdataInterval = setInterval(function()
							{
								if ($('div.no-conversations-selected>span').length === 1 || $('#RSSContainerFull').length === 1)
								{
									clearInterval(AddRSSdataInterval);
									var output = '';
									for (var i = data.length - 1; i >= 0; i--)
									{
										output += createOneRSSitem(data[i]);
									}

									$('div.right-list div.no-conversations-selected').replaceWith(RSScontainer);

									$('div.middle-pane ul.itemlist').html('<div>' + output + '</div>');

									// de-select current conversation item
									$('div[ng-show*=DEFAULT] ul li div.conversation-item.selected').removeClass('selected');

									// change name of RSS feed
									$('#GlobalName').text(GlobalName);

									// add default RSS avatar
									$('#RSSavatar').html('<img class="contact ng-scope" src="' + chrome.extension.getURL('img/rss.png') + '">');

									// scroll to top position
									var elem = $("div.scrollable-feed-list");
									var maxScrollTop = elem[0].scrollHeight - elem.outerHeight();
									$(elem).scrollTop(maxScrollTop);
								}
							}, 100);
						}
						else
						{
							console.log('cex: rss: did not get data from feed URL call.');
						}
					});
				}
				
				// add main conversation item for RSS feed on startup
				var AttachMainItem = setInterval(function()
				{
					if ($( "ul.itemlist.elements li" ).length > 0)
					{
						$('div[ng-show*=DEFAULT] ul').prepend(RSSconvItem);
						chrome.runtime.sendMessage({method: 'RSSdoStartup'}, function(data)
						{
							if (data && data.title && data.item)
							{
								GlobalName = data.title;
								console.log('cex: rss-startup: GlobalName: ' + GlobalName);
								
								$('#RSSTitle').text(GlobalName);
								$('#NewItemTitle').text(data.item);
								if (data.isNew === false) {
									$('#RSSconvItem div.unread-badge').remove();
								}
								$('#RSSconvItem').on('click', function()
								{
									$('#RSSconvItem div.unread-badge').remove();
									showRSSdata();
								});
							}
						});
						
						clearInterval(AttachMainItem);
					}
				}, 500);
				
				// listen for new RSS feed items from background page
				chrome.runtime.onMessage.addListener(
					function(request, sender, sendResponse)
					{
						if (request.method == 'newRSSitems')
						{
							console.log('cex: new RSS items available. adding conversation item');
 
							if ($('#RSSconvItem').length === 1)
							{
								$('#NewItemTitle').text(request.title);
								$('#RSSconvItem div.unread-badge').remove();
								$('#RSSconvItem #MainItemTimestamp').after('<div class="unread-badge"><span class="unread-counter ng-binding">NEW</span></div>');
								
							}
							else {
								console.log('cex: missing main RSS conversation item. unable to show new RSS feed entry');
							}
						}
					}
				);
			}
			else
			{
				console.log('cex: RSS feed URL is not a valid RSS 2.0 feed URL');
			}
		});
	}
});
