// init var
var Blink1ControllerID	= false;
var blink1Enabled		= false;
var blink1StartupDone	= false;
var blink1State			= false;
var acdcEnabled			= false;
var acdcStartupDone		= false;
var acdcAvailable		= false;
var acdcState			= false;

// CircuitBlink1Controller
	//var Blink1ControllerID	= "ehldglaemlolkahaojcpaklgplolcgmc"; // hardcoding to production mode on startup
	chrome.runtime.sendMessage({method: "GetBlink1ControllerID"}, function(data) {
		if (typeof data === 'object' && data.data != undefined)
		{
			chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_blink1"});
			console.log('cex: Blink1 enabled. ' + data.data);
			Blink1ControllerID = data.data;
			blink1Enabled = true;
		}
		blink1StartupDone = true;
	});

// Main loop function
	function initLoop()
	{
		// initializing ACDC
		if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices)
		{
			console.log('cex: Initiated ACDC, set audio device to default.');
			acdcAvailable	= true;
			acdcState		= 'default';
		}

		setInterval(function()
		{
			if (blink1Enabled === true)
			{
				// ping Blink1 controller App to ensure that on disconnect, the Blink1 device will be turned off
				chrome.runtime.sendMessage(Blink1ControllerID, {c: 'ping'});
				
				var tmpUnreadMessages = false;
				$( "ul.itemlist.elements li[ng-repeat*=filterMuted] span.unread-counter" ).each( function(index) {
					var unread = parseInt($( this ).text());
					if (unread > 0) {
						tmpUnreadMessages = true;
						return;
					}
				})
				.promise()
				.done(function()
				{
					if ($("audio").length == 0)
					{
						if ($( "ul.calls li.incoming" ).length > 0)
						{
							if (blink1State != 'incoming')
							{
								chrome.runtime.sendMessage(Blink1ControllerID, {c: 'playPattern'});
								blink1State = 'incoming';
							}
						}
						else if ($( "ul.calls li.outgoing" ).length > 0)
						{
							if (blink1State != 'outgoing')
							{
								chrome.runtime.sendMessage(Blink1ControllerID, {c: 'fadeRgb', r: '255', g: '255', b: '0'});
								blink1State = 'outgoing';
							}
						}
						else if ($( "ul.calls li.started" ).length > 0)
						{
							if (blink1State != 'started')
							{
								chrome.runtime.sendMessage(Blink1ControllerID, {c: 'fadeRgb', r: '255', g: '255', b: '0'});
								blink1State = 'started';
							}
						}
						else if (tmpUnreadMessages)
						{
							if (blink1State != 'unread')
							{
								chrome.runtime.sendMessage(Blink1ControllerID, {c: 'fadeRgb', r: '0', g: '255', b: '0'});
								blink1State = 'unread';
							}
						}
						// if we receive a new message in the highlighted conversation, it might get auto-read.
						// By checking if window has focus, we do not automatically turn the LED off again on receiving messages in the same conversation
						else if (window.document.hasFocus() === true)
						{
							if (blink1State != 'off')
							{
								chrome.runtime.sendMessage(Blink1ControllerID, {c: 'fadeRgb', r: '0', g: '0', b: '0'});
								blink1State = 'off';
							}
						}
					}
					else if ($("audio").length > 0)
					{
						if (blink1State != 'incall')
						{
							chrome.runtime.sendMessage(Blink1ControllerID, {c: 'fadeRgb', r: '255', g: '0', b: '0'});
							blink1State = 'incall';
						}
					}
				});
			}

			if (acdcEnabled === true && acdcAvailable === true)
			{
				if ($("audio").length > 0)
				{
					if (acdcState != 'talk')
					{
						ACDC('acdc-call');
						acdcState = 'talk';
					}
				}
				else
				{
					if (acdcState != 'default')
					{
						acdcState = 'default';
					}
				}
			}
		}, 250);
	}

// ACDC set sinkId
	var ACDC = function(state) {
		chrome.runtime.sendMessage({method: "getLocalStorage", key: state}, function(response) {
			var AudioElem = document.getElementsByTagName("audio")[0];
			AudioElem.setSinkId(response.data);
		});
	}

// ACDC enabled by user ?
	chrome.runtime.sendMessage({method: "getLocalStorage", key: 'acdc-enabled'}, function(response) {
		if (response.data == 'true') {
			chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_audio_control"});
			console.log('cex: ACDC enabled.');
			acdcEnabled = true;
		}
		acdcStartupDone = true;
	});

// getting audio devices every second to allow hot plugging
// this is just temporary, needs a better solution.
	if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices)
	{
		setInterval(function()
		{
			navigator.mediaDevices.enumerateDevices().then(function(data)
			{
				var audioOutputDevices = [];
				for (var i = 0; i < data.length; i ++)
				{
					if (data[i].kind == 'audiooutput')
					{
						audioOutputDevices.push({"deviceId" : data[i].deviceId, "label" : data[i].label});
					}

				}
				chrome.runtime.sendMessage({method: "writeLocalStorage", data: {key: 'acdc-devices', value: JSON.stringify(audioOutputDevices)}});
			})
		}, 1000);
	}

// wait for Blink1 and ACDC startup status to be done
	var startupProcedure = setInterval(function()
	{
		if (acdcStartupDone === true && blink1StartupDone === true)
		{
			if (acdcEnabled === true || blink1Enabled === true)
			{
				initLoop();
			}
			clearInterval(startupProcedure);
		}
	}, 200);
