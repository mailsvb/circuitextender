$('div.header-icons').prepend('<div id="tmpSpinner" style="opacity: 1; transform: translateZ(0); background-image: url(' + chrome.extension.getURL('img/progress-spinner-sprite.png') + '); width: 32px; height: 32px; background-position: 0 0; animation: connecting-large 1s linear infinite;"></div>');

chrome.runtime.sendMessage('pkjccfakeimgdnpjkeccogcfchjbomih', {method: "getBtDevices"}, function(data) {

	$('#tmpSpinner').remove();
	$('div.header-icons').prepend('<div id="tmpPhoneLogo" style="opacity: 1; background-image: url(' + chrome.extension.getURL('img/phone.png') + '); width: 24px; height: 24px; background-position: 0 0;"></div>');
	console.log('BT: received some data');
	if (typeof data === 'object' && data.data != undefined)
	{
		//console.log(data);
		var output = '<div class="popover bottom in" style="top: 31.5px; left: -300px; display: block; bottom: auto;" id="BTdevicesList"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title ng-hide"></h3><div class="popover-content"><div class="snooze"><div class="title"><span>Is this your Circuit enabled DeskPhone?</span></div><div class="info"><span>Connect it to Circuit</span></div><div class="list"><ul>';
		for (var i = 0; i < data.data.length; i++)
		{
			output += '<li name="' + data.data[i].address + '"><span class="listText" style="line-height:11px;">' + data.data[i].name + '<br /><i style="font-size: 8px;">' + data.data[i].address + '</i></span></li>';
		}

		output += '<li name="00:00:00:00:00:00"><span class="listText">Close</span></li>';
		output += '</ul></div></div></div></div></div>';
		$('body').append(output);

		var leftOffset = document.querySelector("#tmpPhoneLogo").offsetLeft - (document.querySelector("#BTdevicesList").offsetWidth / 2) + 16;
		$("#BTdevicesList").css('left', leftOffset + 'px');

		$('#BTdevicesList ul li').click(function()
		{
			$('#BTdevicesList').remove();
			$('#tmpPhoneLogo').remove();
			$('div.header-icons').prepend('<div id="tmpSpinner" style="opacity: 1; transform: translateZ(0); background-image: url(' + chrome.extension.getURL('img/progress-spinner-sprite.png') + '); width: 32px; height: 32px; background-position: 0 0; animation: connecting-large 1s linear infinite;"></div>');
			
			setTimeout(function()
			{
				$('#tmpSpinner').remove();
			}, 5000);
		});
	}
	else
	{
		console.log(data);
	}
});
