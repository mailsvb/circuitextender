(function() {

  // define the fat list
  // input from: https://en.wikipedia.org/wiki/ISO_3166-2:DE

  var listDE = [
    { code: "BW", name: "Baden-Württemberg"},
    { code: "BY", name: "Bayern|Bavaria" },
    { code: "BE", name: "Berlin" },
    { code: "BB", name: "Brandenburg" },
    { code: "HB", name: "Bremen" },
    { code: "HH", name: "Hamburg" },
    { code: "HE", name: "Hessen|Hesse" },
    { code: "MV", name: "Mecklenburg-Vorpommern|Mecklenburg-Western Pomerania"},
    { code: "NI", name: "Niedersachsen|Lower Saxony" },
    { code: "NW", name: "Nordrhein-Westfalen|North Rhine-Westphalia" },
    { code: "RP", name: "Rheinland-Pfalz|Rhineland-Palatinate" },
    { code: "SL", name: "Saarland" },
    { code: "SN", name: "Sachsen|Saxony" },
    { code: "ST", name: "Sachsen-Anhalt|Saxony-Anhalt" },
    { code: "SH", name: "Schleswig-Holstein" },
    { code: "TH", name: "Thüringen|Thuringia" }
  ];

  listDE.codes = [];
  listDE.names = [];

  // define the names and codes and lookups

  var codeLookup = {};
  var nameLookup = {};

  var country;
  for (var i = 0, len = listDE.length; i < len; i ++) {
    country = listDE[i];
    listDE.codes.push(country.code);
    //listDE.names.push(country.name);
    //codeLookup[country.name.toLowerCase()] = country.code;
    var names_array = country.name.split("|");
    for (var j = 0, lenj = names_array.length; j < lenj; j++) {    
        listDE.names.push(names_array[j]);
        codeLookup[names_array[j].toLowerCase()] = country.code;
    }
    nameLookup[country.code] = country.name;
  }

  // define the lookups

  listDE.name = function name(code) {
    return nameLookup[code.toUpperCase()];
  };

  listDE.code = function code(name) {
    return codeLookup[name.toLowerCase()];
  };

  // export this sucker

  if (typeof module !== "undefined")
    module.exports = listDE;
  else
    this.countryListDE = listDE;

})();
