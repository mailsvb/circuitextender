(function() {
  // input from: https://en.wikipedia.org/wiki/ISO_3166-2:US
  // define the fat list

  var listUS = [
    { code: "AL", name: "Alabama" },		// state
    { code: "AK", name: "Alaska" },
    { code: "AZ", name: "Arizona" },
    { code: "AR", name: "Arkansas" },
    { code: "CA", name: "California" },
    { code: "CT", name: "Connecticut" },
    { code: "DE", name: "Delaware" },
    { code: "FL", name: "Florida" },
    { code: "GA", name: "Georgia" },
    { code: "HI", name: "Hawaii" },
    { code: "ID", name: "Idaho" },
    { code: "IL", name: "Illinois" },
    { code: "IN", name: "Indiana" },
    { code: "IA", name: "Iowa" },
    { code: "KS", name: "Kansas" },
    { code: "KY", name: "Kentucky" },
    { code: "LA", name: "Louisiana" },
    { code: "ME", name: "Maine" },
    { code: "MD", name: "Maryland" },
    { code: "MA", name: "Massachusetts" },
    { code: "MI", name: "Michigan" },
    { code: "MN", name: "Minnesota" },
    { code: "MS", name: "Mississippi" },
    { code: "MO", name: "Missouri" },
    { code: "MT", name: "Montana" },	
    { code: "NE", name: "Nebraska" },
    { code: "NV", name: "Nevada" },
    { code: "NH", name: "New Hampshire" },
    { code: "NJ", name: "New Jersey" },
    { code: "NM", name: "New Mexico" },
    { code: "NY", name: "New York" },
    { code: "NC", name: "North Carolina" },
    { code: "ND", name: "North Dakota" },	
    { code: "OH", name: "Ohio" },
    { code: "OK", name: "Oklahoma" },
    { code: "OR", name: "Oregon" },
    { code: "PA", name: "Pennsylvania" },
    { code: "RI", name: "Rhode Island" },
    { code: "SC", name: "South Carolina" },
    { code: "SD", name: "South Dakota" },
    { code: "TN", name: "Tennessee" },
    { code: "TX", name: "Texas" },
    { code: "UT", name: "Utah" },
    { code: "VT", name: "Vermont" },
    { code: "VA", name: "Virginia" },
    { code: "WA", name: "Washington" },
    { code: "WV", name: "West Virginia" },
    { code: "WI", name: "Wisconsin" },
    { code: "WY", name: "Wyoming" },				// state
    { code: "DC", name: "District of Columbia" },	// district
    { code: "AS", name: "American Samoa" },					// outlying territory
    { code: "GU", name: "Guam" },
    { code: "MP", name: "Northern Mariana Islands" },
    { code: "PR", name: "Puerto Rico" },
    { code: "UM", name: "United Stated Minor Outlying Islands" },
    { code: "VI", name: "Virgin Islanda, U.S." }
  ];

  listUS.codes = [];
  listUS.names = [];

  // define the names and codes and lookups

  var codeLookup = {};
  var nameLookup = {};

  var country;
  for (var i = 0, len = listUS.length; i < len; i ++) {
    country = listUS[i];
    listUS.codes.push(country.code);
    listUS.names.push(country.name);
    codeLookup[country.name.toLowerCase()] = country.code;
    nameLookup[country.code] = country.name;
  }

  // define the lookups

  listUS.name = function name(code) {
    return nameLookup[code.toUpperCase()];
  };

  listUS.code = function code(name) {
    return codeLookup[name.toLowerCase()];
  };

  // export this sucker

  if (typeof module !== "undefined")
    module.exports = listUS;
  else
    this.countryListUS = listUS;

})();
