(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-65754544-3', 'auto');
ga('set', 'checkProtocolTask', function(){});
ga('require', 'displayfeatures');
ga('send', 'pageview', '/options.html');

$(function() {
	function eachField(f) {
		$(".text").each(function(_, elem) {
			var elem = $(elem);
			f(elem);
		});
	}

	function restore() {
		applyIntClockDefaultConfig();

		eachField(function(elem) {
			var name = elem.attr("name");
			console.log('options.js:  restore() name=' + name + ', value=' + localStorage[name] )
			elem.attr( "value", localStorage[name]);
		});

		// set audioOutputDevices
		fillAudioOutputDevicesWithContent();

		// apply default weather settings
		applyWeatherConfig();

		// get weather enabled status
		if(localStorage['weather-enabled'] == 'true') {
			$("#weather-enabled").prop('checked', true);
		}else{
			$("#weather-enabled").prop('checked', false);
		}

		// get international clock status
		if(localStorage['intclocks-enabled'] == 'true') {
			$("#intclocks-enabled").prop('checked', true);
		}else{
			$("#intclocks-enabled").prop('checked', false);
		}
		fillIntClockSelectWithTimezonesAndRestoreSelected();

		// get Blink1 status
		/*
		if(localStorage['blink1-enabled'] == 'true') {
			$("#blink1-enabled").prop('checked', true);
		}else{
			$("#blink1-enabled").prop('checked', false);
		}
		*/

		// get rss status
		if(localStorage['rss-enabled'] == 'true') {
			$("#rss-enabled").prop('checked', true);
		}else{
			$("#rss-enabled").prop('checked', false);
		}

		// get rss feed URL
		$("#rss-feed").val(localStorage['rss-feed'])

		// get Notes status
		if(localStorage['notes-enabled'] == 'true') {
			$("#notes-enabled").prop('checked', true);
		}else{
			$("#notes-enabled").prop('checked', false);
		}

		// get preview status
		if(localStorage['preview-enabled'] == 'true') {
			$("#preview-enabled").prop('checked', true);
		}else{
			$("#preview-enabled").prop('checked', false);
		}

		// get Hotword status
		if(localStorage['hotword-enabled'] == 'true') {
			$("#hotword-enabled").prop('checked', true);
		}else{
			$("#hotword-enabled").prop('checked', false);
		}

		// get ACDC status
		if(localStorage['acdc-enabled'] == 'true') {
			$("#acdc-enabled").prop('checked', true);
		}else{
			$("#acdc-enabled").prop('checked', false);
		}

		// get ACDC media file status
		/*
		if(localStorage['acdc-mediafiles'] == 'true') {
			$("#acdc-mediafiles").prop('checked', true);
		}else{
			$("#acdc-mediafiles").prop('checked', false);
		}
		*/

		// disable ACDC in options menu if browser not capable
		if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices)
		{
			$("#acdc-call").prop('disabled', true);
			$("#acdc-enabled").prop('disabled', true);
		}

		// run update after enabling/disabling settings to enable/disable option fields
		update();
	}

	function save() {
		eachField(function(elem) {
			var name = elem.attr("name");
			localStorage[name] = elem.val();
			console.log('options.js: save #1  name=' + name + ', value=' + elem.val() );
		});

		// save international clock status
		var cexIntClocksEnabled = $("#intclocks-enabled").is(':checked') ? 'true' : 'false';
		localStorage['intclocks-enabled'] = cexIntClocksEnabled;
		// create the intclocks-config array
		var tzFormat = localStorage['intclocks-format'];
		var zones = [];
		for (var index = 0; index < 5; index++) {
			var zone = $("#intclocks-zone" + index + "-tz option:selected");
			var name = localStorage['intclocks-zone' + index + '-name']
			if ( zone.val() != "none") {
				localStorage['intclocks-zone' + index + '-tz'] = zone.val();
				zones.push({ 'name': name, 'timezone': zone.val(), 'format': tzFormat});
			} else {
				localStorage.removeItem('intclocks-zone' + index + '-tz');
			}
		}
		localStorage['intclocks-config'] = JSON.stringify(zones);


		// save Blink1 status
		/*
		var cexBlink1Enabled = $("#blink1-enabled").is(':checked') ? 'true' : 'false';
		localStorage['blink1-enabled'] = cexBlink1Enabled;
		*/

		// save rss status
		var cexRssEnabled = $("#rss-enabled").is(':checked') ? 'true' : 'false';
		localStorage['rss-enabled'] = cexRssEnabled;

		// save rss feed URL
		localStorage['rss-feed'] = $("#rss-feed").val();

		// save Notes status
		var cexNotesEnabled = $("#notes-enabled").is(':checked') ? 'true' : 'false';
		localStorage['notes-enabled'] = cexNotesEnabled;

		// save preview status
		var cexPreviewEnabled = $("#preview-enabled").is(':checked') ? 'true' : 'false';
		localStorage['preview-enabled'] = cexPreviewEnabled;

		// save Hotword status
		var cexHotwordEnabled = $("#hotword-enabled").is(':checked') ? 'true' : 'false';
		localStorage['hotword-enabled'] = cexHotwordEnabled;

		// save ACDC status
		var cexACDCEnabled = $("#acdc-enabled").is(':checked') ? 'true' : 'false';
		localStorage['acdc-enabled'] = cexACDCEnabled;

		// save ACDC media files status
		//var cexACDCEnabled = $("#acdc-mediafiles").is(':checked') ? 'true' : 'false';
		//localStorage['acdc-mediafiles'] = cexACDCEnabled;

		// save ACDC devices
		var cexACDCIncall = $("#acdc-call option:selected");
		if (cexACDCIncall.val() != "none")
		{
			localStorage['acdc-call'] = cexACDCIncall.val();
		}
		/*
		var cexACDCDefault = $("#acdc-default option:selected");
		if (cexACDCDefault.val() != "none")
		{
			localStorage['acdc-default'] = cexACDCDefault.val();
		}
		*/

		// save weather format / language
		localStorage['weather-format'] = $("#weather-format option:selected").val();
		localStorage['weather-language'] = $("#weather-language option:selected").val();
		// save weather status
		var cexweatherEnabled = $("#weather-enabled").is(':checked') ? 'true' : 'false';
		localStorage['weather-enabled'] = cexweatherEnabled;
		// save weather API key
		localStorage['weather-api-key'] = $("#weather-api-key").val().replace(/[^a-z0-9]*/g, '');
	}

	function update(){
		// disable clocks field is feature disabled
		if($("#intclocks-enabled").is(':checked')){
			$("#intclocks-format").removeAttr("disabled");
			for (var index = 0; index < 5; index++) {
				$("#intclocks-zone" + index + "-name").removeAttr("disabled");
				$("#intclocks-zone" + index + "-tz").removeAttr("disabled");
			}
			//$("#intclocks-zone0-name").removeAttr("disabled");
		}else{
			$("#intclocks-format").attr("disabled","disabled");
			//$("#intclocks-zone0-name").attr("disabled","disabled");
			for (var index = 0; index < 5; index++) {
				$("#intclocks-zone" + index + "-name").attr("disabled","disabled");
				$("#intclocks-zone" + index + "-tz").attr("disabled","disabled");
			}
		}

		// disable weather field if feature disabled
		if($("#weather-enabled").is(':checked')) {
			$("#weather-format").removeAttr("disabled");
			$("#weather-language").removeAttr("disabled");
			$("#weather-api-key").removeAttr("disabled");
		}
		else {
			$("#weather-format").attr("disabled","disabled");
			$("#weather-language").attr("disabled","disabled");
			$("#weather-api-key").attr("disabled","disabled");
		}

		// disable audio field if feature disabled
		if($("#acdc-enabled").is(':checked')) {
			$("#acdc-call").removeAttr("disabled");
		}
		else {
			$("#acdc-call").attr("disabled","disabled");
		}

		// disable audio field if feature disabled
		if($("#rss-enabled").is(':checked')) {
			$("#rss-feed").removeAttr("disabled");
		}
		else {
			$("#rss-feed").attr("disabled","disabled");
		}
	}

	function applyIntClockDefaultConfig(){
		if ( localStorage['intclocks-enabled'] == undefined
			&& localStorage['intclocks-format'] == undefined
			&& localStorage['intclocks-zone0-name'] == undefined
			&& localStorage['intclocks-zone0-tz'] == undefined
			)
		{
			console.log('no config found; injecting a default config!');
			// inject a default config
			//localStorage['intclocks-enabled'] = true;
			localStorage['intclocks-format'] = 'HH:mm:ss';
			localStorage['intclocks-zone0-name'] = 'New York';
			localStorage['intclocks-zone0-tz'] = 'America/New_York';
			localStorage['intclocks-zone1-name'] = 'Munich';
			localStorage['intclocks-zone1-tz'] = 'Europe/Berlin';
		}
	}

	function fillIntClockSelectWithTimezonesAndRestoreSelected(){
		var zones = {
			//key : value
			'none' : 'Please select',
			'America/Chicago' : 'America/Chicago',
			'America/Denver' : 'America/Denver',
			'America/Los_Angeles' : 'America/Los Angeles',
			'America/New_York' : 'America/New York',
			'America/Sao_Paulo' : 'America/Sao Paulo',
			'America/Toronto' : 'America/Toronto',
			'America/Vancouver' : 'America/Vancouver',
			'Asia/Colombo' : 'Asia/Colombo',
			'Asia/Tokyo' : 'Asia/Tokyo',
			'Asia/Seoul' : 'Asia/Seoul',
			'Asia/Shanghai' : 'Asia/Shanghai',
			'Asia/Singapore' : 'Asia/Singapore',
			'Asia/Hong_Kong' : 'Asia/Hong Kong',
			'Australia/Sydney' : 'Australia/Sydney',
			'Australia/Melbourne' : 'Australia/Melbourne',
			'Europe/Amsterdam' : 'Europe/Amsterdam',
			'Europe/Athens' : 'Europe/Athens',
			'Europe/Berlin' : 'Europe/Berlin',
			'Europe/Brussels' : 'Europe/Brussels',
			'Europe/Helsinki' : 'Europe/Helsinki',
			'Europe/Istanbul' : 'Europe/Istanbul',
			'Europe/Lisbon' : 'Europe/Lisbon',
			'Europe/London' : 'Europe/London',
			'Europe/Madrid' : 'Europe/Madrid',
			'Europe/Moscow' : 'Europe/Moscow',
			'Europe/Paris' : 'Europe/Paris',
			'Europe/Rome' : 'Europe/Rome',
			'Europe/Stockholm' : 'Europe/Stockholm',
			'Europe/Vienna' : 'Europe/Vienna',
			'Etc/GMT-12' : 'Etc/GMT+12',
			'Etc/GMT-11' : 'Etc/GMT+11',
			'Etc/GMT-10' : 'Etc/GMT+10',
			'Etc/GMT-9' : 'Etc/GMT+9',
			'Etc/GMT-8' : 'Etc/GMT+8',
			'Etc/GMT-7' : 'Etc/GMT+7',
			'Etc/GMT-6' : 'Etc/GMT+6',
			'Etc/GMT-5' : 'Etc/GMT+5',
			'Etc/GMT-4' : 'Etc/GMT+4',
			'Etc/GMT-3' : 'Etc/GMT+3',
			'Etc/GMT-2' : 'Etc/GMT+2',
			'Etc/GMT-1' : 'Etc/GMT+1',
			'Etc/GMT-0' : 'Etc/GMT+0',
			'Etc/GMT' : 'Etc/GMT',
			'Etc/GMT+0' : 'Etc/GMT-0',
			'Etc/GMT+1' : 'Etc/GMT-1',
			'Etc/GMT+2' : 'Etc/GMT-2',
			'Etc/GMT+3' : 'Etc/GMT-3',
			'Etc/GMT+4' : 'Etc/GMT-4',
			'Etc/GMT+5' : 'Etc/GMT-5',
			'Etc/GMT+6' : 'Etc/GMT-6',
			'Etc/GMT+7' : 'Etc/GMT-7',
			'Etc/GMT+8' : 'Etc/GMT-8',
			'Etc/GMT+9' : 'Etc/GMT-9',
			'Etc/GMT+10' : 'Etc/GMT-10',
			'Etc/GMT+11' : 'Etc/GMT-11',
			'Etc/GMT+12' : 'Etc/GMT-12'
		};

		for (var index = 0; index < 5; index++) {
			var selectedVal = localStorage['intclocks-zone' + index + '-tz']
			var select = $("#intclocks-zone" + index + "-tz");
			$('option', select).remove();
			$.each(zones, function(text, key) {
				var option = new Option(key, text);
				if (selectedVal != undefined && text == selectedVal) { option.selected = true;}
				select.append($(option));
			});
		}
	}

	function fillAudioOutputDevicesWithContent(){

		if (localStorage['acdc-devices'] != undefined)
		{
			var selectedcall = localStorage['acdc-call'];
			//var selecteddefault = localStorage['acdc-default'];
			var devices = JSON.parse(localStorage['acdc-devices']);
			var selectcall = $("#acdc-call");
			//var selectdefault = $("#acdc-default");
			$('option', selectcall).remove();
			//$('option', selectdefault).remove();
			$.each(devices, function(key, value) {
				var optioncall = new Option(value.label, value.deviceId);
				//var optiondefault = new Option(value.label, value.deviceId);
				if (selectedcall != undefined && value.deviceId == selectedcall) { optioncall.selected = true;}
				//if (selecteddefault != undefined && value.deviceId == selecteddefault) { optiondefault.selected = true;}
				selectcall.append($(optioncall));
				//selectdefault.append($(optiondefault));
			});
		}
	}

	function applyWeatherConfig(){
		if ( localStorage['weather-format'] == undefined && localStorage['weather-language'] == undefined)
		{
			console.log('cex: no weather config found, injecting default config');
			localStorage['weather-format'] = 'metric';
			localStorage['weather-language'] = 'en';
		}
		if ( localStorage['weather-api-key'] == undefined )
		{
			localStorage['weather-api-key'] = '0';
		}

		$("#weather-format").val(localStorage['weather-format']);
		$("#weather-language").val(localStorage['weather-language']);
		$("#weather-api-key").val(localStorage['weather-api-key']);
	}

	restore();

	$("input, select").bind("change",function(e){
		update();
		save();
		localStorage['pendingChanges'] = 'true';
	})

	$("#form").bind("submit", function(e) {
		window.close();
	});

	// show first section on start
	$(".section-header:first").next('div').show();
	$(".section-header:first").next('div').height(260);

	// toggle all sections
	$(".section-header > nobr > span").click(function()
	{
		ga('send', 'event', $(this).text(), 'clicked');
		$(".section-header").next('div').each(function() {
	    $(this).hide();
  	});
		$(this).closest('div').next('div').show();
		$(this).closest('div').next('div').height(260);
	});
});
