chrome.runtime.sendMessage({method: "getLocalStorage", key: 'preview-enabled'}, function(response) {
  if (response.data == 'true') {
    console.log('cex: Full message preview enabled.');
	chrome.runtime.sendMessage({method: "analytics", data: "ENABLED_message_preview"});
    toastr.options = {
    	"closeButton": false,
    	"debug": false,
    	"newestOnTop": true,
    	"progressBar": true,
    	"positionClass": "toast-bottom-right",
    	"preventDuplicates": false,
    	"showDuration": "300",
    	"hideDuration": "1000",
    	"timeOut": "10000",
    	"extendedTimeOut": "10000",
    	"showEasing": "swing",
    	"hideEasing": "linear",
    	"showMethod": "fadeIn",
    	"hideMethod": "fadeOut"
    }

    var AttachPreviewObserverInterval = setInterval(function()
    {
    	if ($('div[ng-show*=DEFAULT] ul li[ng-repeat*=conversations]').length > 0)
    	{
    		console.log('cex: attaching interval for message preview feature');
    		clearInterval(AttachPreviewObserverInterval);
        var PreviewMessage = setInterval(function()
    		{
          if ($('div[ng-show*=DEFAULT] ul li:hover').length > 0 && parseInt($('div[ng-show*=DEFAULT] ul li:hover span.unread-counter').text()) > 0)
          {
            var message = ($('div[ng-show*=DEFAULT] ul li:hover').find('span.text-charcoal50').text().trim() == "") ? $('div[ng-show*=DEFAULT] ul li:hover').find('span.text-charcoal[ng-show*="displayItem.text"]').text().replace(/\s-\s/, "").trim() : $('div[ng-show*=DEFAULT] ul li:hover').find('span.text-charcoal50').text().trim();
      			if (message != '')
      			{
              var goOn = true;
              $('#toast-container div.toast-message').each(function () {
                if ($(this).text() == message) {
                  goOn = false;
                }
              });
              if (goOn)
              {
        				if ($('div[ng-show*=DEFAULT] ul li:hover').find('span.text-charcoal').first().hasClass('ng-hide') === true)
        				{
                  chrome.runtime.sendMessage({method: "analytics", data: "DISPLAY_message_preview"});
        					var header  = $('div[ng-show*=DEFAULT] ul li:hover').first().find('span[ng-show*="!displayItem.sentByMe"]').text().trim();
        						  header = header.replace(/\x3a$/, '');
        						  header = header.replace(/\x2d$/, '');
                  toastr.info(message, header);
        				}
              }
      			}
          }
    		}, 500);
    	}
    }, 500);
  }
});
