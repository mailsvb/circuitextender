$(document).ready(function() {
	// combination for send message
		Mousetrap.bindGlobal('alt+s',	function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_send_alt_s"});
			$( "button[ng-click='onPostAction()']" ).click();
		});
		Mousetrap.bindGlobal('ctrl+return',	function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_send_ctrl_return"});
			$( "button[ng-click='onPostAction()']" ).click();
		});

	// combination for enabling rich text mode
		Mousetrap.bindGlobal('alt+r', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_richtext"});
			if ($('div.text-container').length > 1 && $('div.text-container.focused').length == 1)
			{
				$( "div.text-container.focused" ).next().find( "div[ng-click='enableRichText()']" ).click();
				$( "div.text-container.focused" ).next().find( "input[ng-model='inputData.subject']" ).focus();
			}
			else {
				$( "div[ng-click='enableRichText()']" ).click();
				$( "input[ng-model='inputData.subject']" ).focus();
			}
		});

	// switch to first non muted conversation having unread items
		Mousetrap.bindGlobal('alt+u', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_unread"});
			$( "ul.itemlist.elements.ng-scope li[ng-repeat*=filterMuted] span.unread-counter" ).each( function(index) {
				var unread = parseInt($( this ).text());
				if (unread > 0) {
					console.log('found a conversation having ' + unread + ' unread messages');
					$( this ).click();
					return false;
				}
			});
		});

	// ESC removes text in input box
		Mousetrap.bindGlobal('esc', function(e)
		{
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_esc"});
			if ($( "button[ng-click*=onCancelAction]" ).length > 0)
			{
				$( "button[ng-click*=onCancelAction]" ).click();
			}
			else
			{
				if ($( "div#input-field" ).text() == "")
				{
					$("div.clear-placeholder div[ng-click*=unfocus]").click();
				}
				else
				{
					$( "div#input-field" ).text("");
				}
			}
		});

	// combination for toggling mute status of conversation
		Mousetrap.bindGlobal('alt+m', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_mute_conversation"});
			var ToggleMuteByKeyStroke = setInterval(function(){
			if ($( "div[ng-click^=toggleMuteConversation]" ).length > 0){
				$( "div[ng-click^=toggleMuteConversation]" ).click();
					clearInterval(ToggleMuteByKeyStroke);
				}
			},100);
			$( "div[ng-click*=details-member]" ).click();
		});

	// combination for toggling mute status of active call
		Mousetrap.bindGlobal('alt+x', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_mute_call"});
			if ($("audio").attr('ng-src') != "")
			{
				$('div[id=call-action-bar] div.microphone div[class*=muted]').click();
			}
		});

	// combination for switch to default conversations
		Mousetrap.bindGlobal('alt+1', function(e) { $( "div.tab-bar-item.ng-scope[ng-click*=DEFAULT]" ).click(); });
	// combination for switch to muted conversations
		Mousetrap.bindGlobal('alt+2', function(e) { $( "div.tab-bar-item.ng-scope[ng-click*=MUTED]" ).click(); });
	// combination for switch to muted conversations
		Mousetrap.bindGlobal('alt+3', function(e) { $( "div.tab-bar-item.ng-scope[ng-click*=OPEN]" ).click(); });
	// combination for toggling 3rd column
		Mousetrap.bindGlobal('alt+f', function(e)
		{
			e.preventDefault();
			$( "div.toggle-column" ).click();
		});
	// combination for focusing search bar
		Mousetrap.bindGlobal('alt+q', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_search"});
			$( "input[u-set-focus=searchField]" ).focus();
		});
	// start audio call
		Mousetrap.bindGlobal('space space', function(e) {
			chrome.runtime.sendMessage({method: "analytics", data: "SHORTCUT_start_audio_call"});
			if ($( "div.action-button.start.voice" ).length > 0)
			{
				$( "div.action-button.start.voice" ).click();
			}
			else if ($( "div.action-button.hangup" ).length > 0)
			{
				$( "div.action-button.hangup" ).click();
			}
		});

	// focus input field
		Mousetrap.bind(['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
						'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
						'0','1','2','3','4','5','6','7','8','9','ä','ö','ü','Ä','Ö','Ü'], function(e) {
			$( "div#input-field" ).focus();
		});
});
