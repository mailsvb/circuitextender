// loading analytics libs
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-65754544-4', 'auto');
ga('set', 'checkProtocolTask', function(){});
ga('require', 'displayfeatures');
ga('send', 'pageview', '/circuit.html');

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	

	// generic get request
	if (request.method == "getLocalStorage") {
		var value = localStorage[request.key];
		sendResponse({data: value});
		//console.log('background.js: getLocalStorage: key=' + request.key + " --> " + value);
	}

	// generic write request
	else if (request.method == "writeLocalStorage") {
		//console.log('background.js: writeLocalStorage: ' + request.data.key + ':' + request.data.value);
		localStorage[request.data.key] = request.data.value;
	}

	// 'international clocks' specific config request
	else if (request.method == "getIntClocksConfig") {
		var format = localStorage['intclocks-format'];
		var config = JSON.parse(localStorage['intclocks-config']);
		sendResponse(config);
		console.log('background.js: getIntClocksConfig: response data=' + localStorage['intclocks-format']);
	}

	// get weather info
	else if (request.method == "getWeather") {
		console.log('background.js: getWeather: request.data: ' + request.data);
		// define default values
		var weatherLang		= "en";
		var weatherUnit		= "metric";
		var weatherDisp 	= "°C";
		var weatherAPIkey	= "";

		//console.log('background.js: weather defaults: weatherLang: ' + weatherLang + ', weatherUnit: ' + weatherUnit + ', weatherDisp: ' + weatherDisp + ', weatherAPIkey: ' + weatherAPIkey);

		var temp = localStorage['weather-format'];
		if (temp == 'metric')
		{
			weatherUnit = "metric";
			weatherDisp = "°C";
		}
		else
		{
			weatherUnit = "imperial";
			weatherDisp = "°F";
		}
		
		temp = localStorage['weather-language'];		
		if (temp != undefined)
		{
			weatherLang = temp;
		}
		
		temp = localStorage['weather-api-key'];		
		if (temp != undefined)
		{
			weatherAPIkey = temp;
		}
		console.log('background.js: weather with user settings: weatherLang: ' + weatherLang + ', weatherUnit: ' + weatherUnit 
			+ ', weatherDisp: ' + weatherDisp + ', weatherAPIkey: ' + weatherAPIkey);		
		
		// OpenWeather:  http://api.openweathermap.org/data/2.5/weather?q=München&units=metric&lang=en&APPID=4f683a2ce57da7a09154cb77825a8c59
		// create request
		// request.data might look like this:   München,Bayern,Germany
		//                      or like this:   Sheffield,United Kingdom
		// https://en.wikipedia.org/wiki/ISO_3166
		// https://en.wikipedia.org/wiki/ISO_3166-2:DE	, :US, :CA, ...	
		var LocParts = request.data.split(",");
		var query = LocParts[0];
		var countryCode = '';
		if(LocParts.length > 2) {
			countryCode = countryList.code(LocParts[2]);
			//console.log('background.js: countryCode: ' + countryCode );
			if (countryCode == 'DE') {
				var subdivisionCode = countryListDE.code(LocParts[1]);
				console.log('background.js: subdivisionCode: ' + subdivisionCode );
			        if (subdivisionCode != undefined ) 
				    countryCode = countryCode + '-' + subdivisionCode;
			} else if (countryCode == 'US') {
				var subdivisionCode = countryListUS.code(LocParts[1]);
				console.log('background.js: subdivisionCode: ' + subdivisionCode );
				countryCode = countryCode + '-' + subdivisionCode;
			}
		} else if(LocParts.length > 1) {
			countryCode = countryList.code(LocParts[1]);
		}
		if (countryCode != undefined && countryCode != '')			
			query += ',' + countryCode;		
		
		console.log('background.js: query: ' + query );						
		var url = "http://api.openweathermap.org/data/2.5/weather?q=" + query 
			+ "&units=" + weatherUnit + "&lang=" + weatherLang + "&APPID=" + weatherAPIkey;
		console.log('background.js: getWeather: url: ' + url);

		$.get( url, function( data ) {
			console.log('background.js: getWeather: data: ' + JSON.stringify(data));
			if (data )
				if ( data.cod == 200)
				{
					var result = data.weather[0].description + ', ' + Math.round(data.main.temp) + weatherDisp
					console.log('background.js: getWeather: result: ' + result);
					sendResponse({query: query, result: result});
				} else {
					sendResponse({query: query, cod: data.cod, error_message: data.message});
				}
		}).fail(function() {
			console.warn('background.js: openweathermap request failed');
		});
		return true;
	}

	// get Blink(1)ContorllerStatus
	else if (request.method == "GetBlink1ControllerID") {
		chrome.management.get('ehldglaemlolkahaojcpaklgplolcgmc', function(res)
		{
			if (chrome.runtime.lastError) {
				console.log('Blink1 Controller not installed from webstore, checking DEV version');
				chrome.management.get('hocckaigfmgdmnpakchgefagfjbcdgop', function(res)
				{
					if (chrome.runtime.lastError) {
						console.log('Blink1 Controller not installed, deactivating Blink1 feature');
						sendResponse(false);
					}
					else
					{
						console.log('Blink1 Controller installed. Setting Controller App Id to development mode');
						sendResponse({data: 'hocckaigfmgdmnpakchgefagfjbcdgop'});
					}
				});
			}
			else
			{
				console.log('Blink1 Controller installed. Setting Controller App Id to production mode');
				sendResponse({data: 'ehldglaemlolkahaojcpaklgplolcgmc'});
			}
		});
		return true;
	}

	// RSS startup
	else if (request.method == 'RSSdoStartup')
	{
		console.log("cex: rss: RSSdoStartup: sent from tab.id=", sender.tab.id);
		localStorage['rss-ServerTabId'] = sender.tab.id;

		// setting default RSS feed address if non is set
		if (localStorage['rss-feed'] == undefined || localStorage['rss-feed'] == '')
		{
			localStorage['rss-feed'] = 'http://feeds.reuters.com/reuters/topNews?format=rss2.0';
		}
		// loading current feed address from storage
		var FeedAddress = localStorage['rss-feed'];

		$.get(FeedAddress, function(data) {
			var $xml 			= $(data);
			var RSSFeedTitle	= $xml.find("channel > title").text().trim();
			var RSSFeedItems 	= $(data).find("item");
			var FirstItem		= $(RSSFeedItems[0]).find("title").text().trim();
			if (localStorage['rss-lastItem'] != FirstItem) {
				localStorage['rss-lastItem'] = FirstItem;
				sendResponse({title: RSSFeedTitle, item: FirstItem, isNew: true});
			} else {
				sendResponse({title: RSSFeedTitle, item: FirstItem, isNew: false});
			}
		});

		var runTimeInterval = setInterval(function()
		{
			//console.log('cex: rss: polling ...');
			$.get(FeedAddress, function(data) {
				//console.log('cex: rss: got data: ' + data);
				var xml 			= $(data).find("item");
				var currentTitle	= $(xml[0]).find("title").text().trim();

				if (localStorage['rss-lastItem'] != currentTitle) {
					// tab (to send to) still available ?
					var tabFound = false;
					var tabId = parseInt(localStorage['rss-ServerTabId']);
					//console.log('cex: rss: tabId: ' + tabId);
					chrome.tabs.query({title: "*Circuit*"}, function(tabs) {
						for (var i = 0; i < tabs.length; i++) {
					    	//console.log('cex: rss: tab: id:' + tabs[i].id + ', title:' + tabs[i].title);
							if (tabs[i].id == tabId) { tabFound = true; break; }
					    }
						//console.log('cex: rss: tabFound: ' + tabFound);						    
						if (tabFound )
						{
							console.log('cex: rss: NEWS: ' + currentTitle);
							localStorage['rss-lastItem'] = currentTitle;
							chrome.tabs.sendMessage(tabId, {method:"newRSSitems", title: currentTitle});
						}
						else
						{
							console.log('cex: rss: stopping RSS runtime Interval');
							clearInterval(runTimeInterval);
						}
					});

				}


			});
		}, 30000);

		return true;
	}

	// get all RSS data
	else if (request.method == 'RSSgetData')
	{
		$.get(localStorage['rss-feed'], function(data) {
			var ReturnData = [];
			var $xml = $(data);
			$xml.find("item").each(function() {
				item = {
					title: $(this).find("title").text().trim(),
					link: $(this).find("link").text().trim(),
					description: $(this).find("description").text().trim(),
					pubDate: $(this).find("pubDate").text().trim()
				}
				ReturnData.push(item);
			});
			sendResponse(ReturnData);
		});
		return true;
	}

	// save new RSS address (requires seperate method to prove valid RSS address before saving)
	else if (request.method == 'verifyRSSaddress')
	{
		$.get(localStorage['rss-feed'], function(data) {
			var ReturnData = [];
			var $xml = $(data);
			if ($xml.find("channel > title").text() != '' && $xml.find("item").length > 0)
			{
				sendResponse({data: 'true'});
			}
			else
			{
				sendResponse({data: 'false'});
			}
		});
		return true;
	}

	// generic write request
	else if (request.method == "analytics") {
		ga('send', 'event', request.data, 'used');
	}

	// else
	else {
		console.log('background.js:  UNSUPPORTED request.method=' + request.method );
		sendResponse({}); // snub them.
	}
});
